-- Fix compatibility for settings-related changes
-- Only loaded if the versions of the mod and the world mismatch

local function version_is_lower(v1, v2)
	local d1, c1, d2, c2
	while #v1 > 0 and #v2 > 0 do
		d1, c1, v1 = v1:match("^(%d*)(%D*)(.*)$")
		d2, c2, v2 = v2:match("^(%d*)(%D*)(.*)$")

		d1 = tonumber(d1) or -1
		d2 = tonumber(d2) or -1
		if d1 ~= d2 then
			return d1 < d2
		end
		if c1 ~= c2 then
			return c1 < c2
		end
	end
	return false
end

local function fix_min_catchment(settings, is_global)
	local prefix = is_global and "mapgen_rivers_" or ""

	local min_catchment = settings:get(prefix.."min_catchment")
	if min_catchment then
		min_catchment = tonumber(min_catchment)
		local blocksize = tonumber(settings:get(prefix.."blocksize") or 15)
		settings:set(prefix.."min_catchment", tonumber(min_catchment) * blocksize*blocksize)
		local max_catchment = settings:get(prefix.."max_catchment")
		if max_catchment then
			max_catchment = tonumber(max_catchment)
			local wpower = math.log(2*blocksize)/math.log(max_catchment/min_catchment)
			settings:set(prefix.."river_widening_power", wpower)
		end
	end
end

local function fix_compatibility_minetest(settings)
	local previous_version = settings:get("mapgen_rivers_version") or "0.0"

	if previous_version == "0.0" then
		fix_min_catchment(settings, true)
	end

	if version_is_lower(previous_version, "1.0.2-dev1") then
		local blocksize = tonumber(settings:get("mapgen_rivers_blocksize") or 15)
		local grid_x_size = tonumber(settings:get("mapgen_rivers_grid_x_size"))
		if grid_x_size then
			settings:set("mapgen_rivers_map_x_size", tostring(grid_x_size * blocksize))
		end
		local grid_z_size = tonumber(settings:get("mapgen_rivers_grid_z_size"))
		if grid_z_size then
			settings:set("mapgen_rivers_map_z_size", tostring(grid_z_size * blocksize))
		end
	end
end

local function fix_compatibility_mapgen_rivers(settings)
	local previous_version = settings:get("version") or "0.0"

	if previous_version == "0.0" then
		fix_min_catchment(settings, false)
	end

	if version_is_lower(previous_version, "1.0.2-dev1") then
		local blocksize = tonumber(settings:get("blocksize") or 15)
		local grid_x_size = tonumber(settings:get("grid_x_size"))
		if grid_x_size then
			settings:set("map_x_size", tostring(grid_x_size * blocksize))
		end
		local grid_z_size = tonumber(settings:get("grid_z_size"))
		if grid_z_size then
			settings:set("map_z_size", tostring(grid_z_size * blocksize))
		end
	end
end

return fix_compatibility_minetest, fix_compatibility_mapgen_rivers
