-- Main file, calls the other files and triggers main functions

mapgen_rivers = {}

local modpath = minetest.get_modpath(minetest.get_current_modname()) .. '/'
mapgen_rivers.modpath = modpath
mapgen_rivers.world_data_path = minetest.get_worldpath() .. '/river_data/'

dofile(modpath .. 'settings.lua')
dofile(modpath .. 'gridmanager.lua')
dofile(modpath .. 'gridio.lua')
dofile(modpath .. 'polygons.lua')
dofile(modpath .. 'heightmap.lua')
dofile(modpath .. 'mapgen.lua')

minetest.register_on_mods_loaded(function()
	local exist = mapgen_rivers.try_load_grid()

	if not exist then -- If grid does not exist yet, generate it
		dofile(modpath .. 'pregenerate.lua')

		local grid = mapgen_rivers.generate_grid()
		mapgen_rivers.write_grid(grid)
		mapgen_rivers.try_load_grid(grid) -- Reload if needed
	end
end)
