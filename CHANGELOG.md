CHANGELOG
=========

## 'v1.0.3' (2023-11-08)
- Add world name to view_map.py
- Adjust default settings for 62000 x 62000 nodes maps (==> some 80 nodes width rivers)
- Change biomegen to hard dependence
- Introduce new setting min_river_width_factor to compensate narrow rivers discontinuity produced by distort
- Bury rivers 1 node to prevent water overflow
- Initialize random seed in pregenerate for reproducible maps
- Gaël: Lots of optimizations, code refactoring and comments
- Gaël: Fix river shape in confluences (less sharp riverbeds when a small rivers joins a big one)
- Gaël: Added margin with a settable width near grid border
- Gaël: Remove 'default' hard dependency

## `v1.0.2` (2022-01-10)
- Use builtin logging system and appropriate loglevels
- Skip empty chunks, when generating high above ground (~20% speedup)
- Minor optimizations (turning global variables to local...)

## `v1.0.1` (2021-09-14)
- Automatically switch to `singlenode` mapgen at init time

## `v1.0` (2021-08-01)
- Rewritten pregen code (terrainlib) in pure Lua
- Optimized grid loading
- Load grid nodes on request by default
- Changed river width settings
- Added map size in settings
- Added logs
